
/*
 * This is an auto generated Run-Time-Environment Component Configuration File
 * DO NOT MODIFY!
 *
 * Project: 'stm32f070_iar_0618'
 * Device: 'ARMCM0' Pack: 'ARM::CMSIS.5.3.0'
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

#define CMSIS_device_header "ARMCM0.h"
#define RTE_CMSIS_RTOS2                 /* CMSIS-RTOS2 */
        
#define RTE_CMSIS_RTOS2_RTX5            /* CMSIS-RTOS2 Keil RTX5 */

#endif  /* RTE_COMPONENTS_H */

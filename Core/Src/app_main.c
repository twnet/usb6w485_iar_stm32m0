

#include "cmsis_os2.h"
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_gpio.h"

// STM32 HAL USB Driver middleware header
#include "usb_device.h"
#include "usbd_hid.h"
#include "usbd_composite.h"
#include "usbd_cdc_if.h"

// thread for led
#include "thread_blink_led.h"
// thread for uart
#include "thread_uart.h"

#include "ring_fifo.h"

extern UART_HandleTypeDef huart1;
extern DMA_HandleTypeDef hdma_usart1_rx;

#define APP_TX_DATA_SIZE 512
extern uint8_t UserTxBufferFS[APP_TX_DATA_SIZE];
//uint8_t data[2*RX_BUFFER_SIZE] = {'\0'};   /* Data buffer that contains newly received data */
uint8_t *report_data;

    /*----------------------------------------------------------------------------
 *      Main: Initialize and start RTX Kernel
 *---------------------------------------------------------------------------*/
    void
    app_main(void *arg)
{

    //	tid_phaseLED0 = osThreadNew(thread_LED0_blink, NULL, NULL);
    //	tid_phaseLED1 = osThreadNew(thread_LED1_blink, NULL, NULL);
    //	tid_clock = osThreadNew(thread_clock, NULL, NULL);
    //  osThreadFlagsSet(tid_phaseA, 0x0001);          /* set signal to phaseA thread   */
    //report_data = (uint8_t*) malloc(2);

    // tid_blink_tx = osThreadNew(thread_blink_tx, NULL, &worker_attr_1);
    // tid_blink_rx = osThreadNew(thread_blink_rx, NULL, &worker_attr_2);
    // tid_blink_isr_tx = osThreadNew(thread_blink_isr_tx, NULL, &worker_attr_3);
    // tid_blink_isr_rx = osThreadNew(thread_blink_isr_rx, NULL, &worker_attr_4);

    tid_blink_led = osThreadNew(thread_blink_led, NULL, &worker_attr_1);
    tid_uart = osThreadNew(thread_uart, NULL, &worker_attr_2);

    // Initialize report data buffer
    report_data = (uint8_t *)malloc(4);
    report_data[0] = 1;
    report_data[1] = 1;

    osDelay(5000);

    // Start DMA receive
    if (HAL_UART_Receive_DMA(&huart1, rxBuffer, 256) != HAL_OK)
    {
        Error_Handler();
    }

    // Enable uart idle line Interrupt
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
    // Enable transmit finish Interrupt
    //__HAL_DMA_ENABLE_IT(&hdma_usart1_rx, DMA_IT_TC);

    /* Disable Half Transfer Interrupt */
    //__HAL_DMA_DISABLE_IT(huart1.hdmarx, DMA_IT_HT);

    while (1)
    {
        //HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
        //CDC_Transmit_FS("AAAA", 4);
        //USBD_HID_SendReport(&hUsbDeviceFS, report_data, 2);

        osDelay(10000);
    }

    return;
}

void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{

    uint16_t i, pos, start, length;

    if (mutex_uart_rx == 1)
    { // Thread busy
        return;
    }

    dma_uart_rx.currCNDTR = __HAL_DMA_GET_COUNTER(huart->hdmarx);

    // Idle Event or Full event
    start = RX_BUFFER_SIZE - dma_uart_rx.prevCNDTR;
    length = (dma_uart_rx.currCNDTR > dma_uart_rx.prevCNDTR) ? dma_uart_rx.prevCNDTR + RX_BUFFER_SIZE - dma_uart_rx.currCNDTR : dma_uart_rx.prevCNDTR - dma_uart_rx.currCNDTR;
    dma_uart_rx.prevCNDTR = dma_uart_rx.currCNDTR;
    /* Copy and Process new data */
    for (i = 0, pos = start; i < length; ++i, ++pos)
    {
        FIFO_push(&uart_rx_data_fifo, rxBuffer[pos]);
    }

    osThreadFlagsSet(tid_blink_led, TH_FG_BLINK_RX);

    osThreadFlagsSet(tid_uart, TH_FG_RX_DATA_READY);
    //CDC_Transmit_FS(UserTxBufferFS, length);

    return;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

    uint16_t i, pos, start, length;

    if (mutex_uart_rx == 1)
    { // Thread busy
        return;
    }

    dma_uart_rx.currCNDTR = __HAL_DMA_GET_COUNTER(huart->hdmarx);

    // Idle Event or Full event
    start = RX_BUFFER_SIZE - dma_uart_rx.prevCNDTR;
    length = (dma_uart_rx.currCNDTR > dma_uart_rx.prevCNDTR) ? dma_uart_rx.prevCNDTR + RX_BUFFER_SIZE - dma_uart_rx.currCNDTR : dma_uart_rx.prevCNDTR - dma_uart_rx.currCNDTR;
    dma_uart_rx.prevCNDTR = dma_uart_rx.currCNDTR;
    /* Copy and Process new data */
    for (i = 0, pos = start; i < length; ++i, ++pos)
    {
        FIFO_push(&uart_rx_data_fifo, rxBuffer[pos]);
    }

    osThreadFlagsSet(tid_blink_led, TH_FG_BLINK_RX);

    osThreadFlagsSet(tid_uart, TH_FG_RX_DATA_READY);
    //CDC_Transmit_FS(UserTxBufferFS, length);

    return;
}
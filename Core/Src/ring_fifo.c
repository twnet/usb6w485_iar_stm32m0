#include "ring_fifo.h"
#include "stdint.h"

/****************** Ring buffer ******************/

void FIFO_init(volatile FIFO* p_fifo_str) {
	// Initialize the FIFO struct
	//memset(p_fifo_str, 0, sizeof(FIFO));
	p_fifo_str->tail = 0;
	p_fifo_str->head = 0;
}

int FIFO_pop(volatile FIFO* p_fifo_str, TYPE_FIFO_DATA* p_data) {
	
	/*** Make sure there is atomic operation at FIFO ***/
	
	// There is empty FIFO
	if(p_fifo_str->tail == p_fifo_str->head) {
		return -1;
	}
	
	uint16_t tail_next = p_fifo_str->tail + 1;
	
	if(tail_next >= SIZE_FIFO) {
		tail_next = 0;
	}
	
	// Get the data at head
	*p_data = p_fifo_str->data[p_fifo_str->tail];
	
	//printf("FIFO pop: %d \n",*p_data);
	//debug_write_data(0);
	//debug_write_data(*p_data);
	
	p_fifo_str->tail = tail_next;

	/**************************************************/
	
	return 0;
}

int FIFO_push(volatile FIFO* p_fifo_str, TYPE_FIFO_DATA data) {
	
	
	/*** Make sure there is atomic operation at FIFO ***/
	
	//printf("FIFO push: %d \n",data);
	//debug_write_data(-1);
	//debug_write_data(data);
	uint16_t head_next = p_fifo_str->head + 1;
	
	// Make sure there is not large than SIZE_FIFO
	if(head_next >= SIZE_FIFO) {
		head_next = 0;
	}
	
	
	if(head_next == p_fifo_str->tail) {
		// The FIFO was full
		return -1;
	}
	
	p_fifo_str->data[p_fifo_str->head] = data;
	p_fifo_str->head = head_next;
	
	/**************************************************/
	
	return 0;
}
/***************************************************/
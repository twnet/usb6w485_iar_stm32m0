#include "thread_uart.h"

#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_gpio.h"
#include "ring_fifo.h"

// Define objects that are statically allocated for worker thread 1
__attribute__((section(".bss.os.thread.cb")))
osRtxThread_t uart_thread_tcb[1];

// Reserve two areas for the stacks of worker thread 1
// uint64_t makes sure the memory alignment is 8
uint64_t worker_thread_stk_2[128];

// Define the attributes which are used for thread creation
// Optional const saves RAM memory and includes the values in periodic ROM tests
const osThreadAttr_t worker_attr_2 = {
    "uart_cdc_thread",
    osThreadDetached,
    &uart_thread_tcb[0],
    sizeof(uart_thread_tcb[0]),
    &worker_thread_stk_2[0],
    sizeof(worker_thread_stk_2),
    osPriorityRealtime,
    0};

// Define ID object for thread
osThreadId_t tid_uart;



uint8_t rxBuffer[RX_BUFFER_SIZE]; /* Circular buffer for DMA */
uint8_t txBuffer[TX_BUFFER_SIZE];

DMA_Event_t dma_uart_rx = {0, RX_BUFFER_SIZE, RX_BUFFER_SIZE};
UART_Data_t uart_data = {0, txBuffer};

volatile uint8_t mutex_uart_tx;
volatile uint8_t mutex_uart_rx;

volatile FIFO uart_rx_data_fifo;
volatile FIFO uart_tx_data_fifo;

extern UART_HandleTypeDef huart1;

// Thread function for blinking
void
thread_uart(void *arg)
{

    

    mutex_uart_tx = 0;
    mutex_uart_rx = 0;
    
    // Initialize the FIFO buffer
    FIFO_init(&uart_tx_data_fifo);
    FIFO_init(&uart_rx_data_fifo);

    for (;;)
    { 
        uint32_t flags = osThreadFlagsWait(
            TH_FG_RX_DATA_READY | TH_FG_TX_DATA_READY, 
            osFlagsWaitAny | osFlagsNoClear, 
            osWaitForever);

        /* Ignore IDLE Timeout when the received characters exactly 
         filled up the DMA buffer and DMA Rx Complete IT is generated, 
        but there is no new character during timeout */
        if(flags & TH_FG_RX_DATA_READY) {
            mutex_uart_rx = 1;
            if (dma_uart_rx.currCNDTR == RX_BUFFER_SIZE)
            {
                // IDLE but no newly data come-in
                mutex_uart_rx = 0;
                osThreadFlagsClear(TH_FG_RX_DATA_READY);
            }
            else
            {
                int data_length = 0;
                // Copy FIFO to USB CDC buffer
                for(int i=0; i< RX_BUFFER_SIZE; i++) {

                    // Pop the fifo buffer, makesure there is not empty
                    if (FIFO_pop(&uart_rx_data_fifo, &(UserTxBufferFS[i])) == -1)
                    {
                        // Empty data
                        break;
                    }
                    data_length++;
                }
                mutex_uart_rx = 0;
                // Sent data to the Host
                CDC_Transmit_FS(UserTxBufferFS, data_length);
                osThreadFlagsClear(TH_FG_RX_DATA_READY);
            }

            
        }
        // if(flags & TH_FG_TX_DATA_READY) {
        //     uart_data.dataLength = 0;
        //     // Copy FIFO to UART TX buffer
        //     for (int i = 0; i < TX_BUFFER_SIZE; i++)
        //     {

        //         // Pop the fifo buffer, makesure there is not empty
        //         if (FIFO_pop(&uart_tx_data_fifo, &(uart_data.pData[i])) == -1)
        //         {
        //             // Empty data
        //             break;
        //         }
        //         uart_data.dataLength++;
        //     }

        //     // Setup  RTS1 ON
        //     HAL_GPIO_WritePin(RTS1_RS485_GPIO_Port, RTS1_RS485_Pin, GPIO_PIN_SET);
        //     //mutex_uart_tx = 1;
        //     // RS485 write data
        //     HAL_UART_Transmit(&huart1, uart_data.pData, uart_data.dataLength, 10);
        //     // Setup RTS1 OFF
        //     //mutex_uart_tx = 0;
        //     HAL_GPIO_WritePin(RTS1_RS485_GPIO_Port, RTS1_RS485_Pin, GPIO_PIN_RESET);
        //     osThreadFlagsClear(TH_FG_TX_DATA_READY);
        // }
        
    }
}

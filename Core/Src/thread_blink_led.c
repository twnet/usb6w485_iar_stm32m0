#include "thread_blink_led.h"

#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_gpio.h"

// Define objects that are statically allocated for worker thread 1
__attribute__((section(".bss.os.thread.cb")))
osRtxThread_t blinker_thread_tcb[1];

// Reserve two areas for the stacks of worker thread 1
// uint64_t makes sure the memory alignment is 8
uint64_t worker_thread_stk_1[32];

// Define the attributes which are used for thread creation
// Optional const saves RAM memory and includes the values in periodic ROM tests
const osThreadAttr_t worker_attr_1 = {
    "blink_led",
    osThreadDetached,
    &blinker_thread_tcb[0],
    sizeof(blinker_thread_tcb[0]),
    &worker_thread_stk_1[0],
    sizeof(worker_thread_stk_1),
    osPriorityLow,
    0};

// Define ID object for thread
osThreadId_t tid_blink_led;

// Thread function for blinking 
void thread_blink_led(void* arg) {

    uint32_t led_tick_tx = 0;
    uint32_t led_tick_rx = 0;
    uint32_t led_tick_isr_tx = 0;
    uint32_t led_tick_isr_rx = 0;
    
    
    for (;;)
    {
      
        osDelay(10);
        // Wait for any blink signal and don't clear the flag
        uint32_t flags = osThreadFlagsWait(TH_FG_BLINK_TX | TH_FG_BLINK_RX | TH_FG_BLINK_ISR_RX | TH_FG_BLINK_ISR_TX, osFlagsWaitAny | osFlagsNoClear, osWaitForever);

        if ((flags & TH_FG_BLINK_TX) && (led_tick_tx == 0))
        {
            led_tick_tx = osKernelGetTickCount();
            HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_RESET);
        }
        if ( (osKernelGetTickCount()-led_tick_tx) > 500) { // Until excess 50 ms. Turn OFF led and clear flag
            led_tick_tx = 0;
            osThreadFlagsClear(TH_FG_BLINK_TX);
            HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, GPIO_PIN_SET);
        }

        if ((flags & TH_FG_BLINK_RX) && (led_tick_rx == 0))
        {
            led_tick_rx = osKernelGetTickCount();
            HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
        }
        if ( (osKernelGetTickCount() - led_tick_rx) > 500)
        { // Until excess 50 ms. Turn OFF led and clear flag
            led_tick_rx = 0;
            osThreadFlagsClear(TH_FG_BLINK_RX);
            HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
        }

        if ((flags & TH_FG_BLINK_ISR_RX) && (led_tick_isr_rx == 0))
        {
            led_tick_isr_rx = osKernelGetTickCount();
            HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
        }
        if ((osKernelGetTickCount() - led_tick_isr_rx) > 500)
        { // Until excess 50 ms. Turn OFF led and clear flag
            led_tick_isr_rx = 0;
            osThreadFlagsClear(TH_FG_BLINK_ISR_RX);
            HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
        }

        if ((flags & TH_FG_BLINK_ISR_TX) && (led_tick_isr_tx == 0))
        {
            led_tick_isr_tx = osKernelGetTickCount();
            HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
        }
        if ((osKernelGetTickCount() - led_tick_isr_tx) > 500)
        { // Until excess 50 ms. Turn OFF led and clear flag
            led_tick_isr_tx = 0;
            osThreadFlagsClear(TH_FG_BLINK_ISR_TX);
            HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
        }


    }
}

#ifndef __RING_FIFO_H__
#define __RING_FIFO_H__

#include "stdint.h"

#define TYPE_FIFO_DATA	uint8_t
#define SIZE_FIFO		(512)

/****************** Ring buffer ******************/
typedef struct{
	volatile uint16_t tail;
	volatile uint16_t head;
	volatile TYPE_FIFO_DATA data[SIZE_FIFO];
} FIFO;

void FIFO_init(volatile FIFO *p_fifo_str);

int FIFO_pop(volatile FIFO *p_fifo_str, TYPE_FIFO_DATA *p_data);

int FIFO_push(volatile FIFO *p_fifo_str, TYPE_FIFO_DATA data);
/***************************************************/

#endif // __RING_FIFO_H__
#ifndef __THREAD_BLINK_LED_H__
#define __THREAD_BLINK_LED_H__

// RTX OS for types of RTX object3s
#include "rtx_os.h"

// Define objects that are statically allocated for worker thread 1
__attribute__((section(".bss.os.thread.cb")))
extern osRtxThread_t blinker_thread_tcb[1];

// Reserve two areas for the stacks of worker thread 1
// uint64_t makes sure the memory alignment is 8
extern uint64_t worker_thread_stk_1[32];

// Define the attributes which are used for thread creation
// Optional const saves RAM memory and includes the values in periodic ROM tests
extern const osThreadAttr_t worker_attr_1;

// Define ID object for thread 
extern osThreadId_t tid_blink_led;

#define TH_FG_BLINK_TX      (0x0001)
#define TH_FG_BLINK_RX      (0x0002)
#define TH_FG_BLINK_ISR_RX  (0x0004)
#define TH_FG_BLINK_ISR_TX  (0x0008)

void thread_blink_led(void* arg);

#endif // __THREAD_BLINK_LED
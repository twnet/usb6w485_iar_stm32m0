#ifndef __THREAD_UART_H__
#define __THREAD_UART_H__

// RTX OS for types of RTX object3s
#include "rtx_os.h"
#include "ring_fifo.h"

// Define objects that are statically allocated for worker thread 1
__attribute__((section(".bss.os.thread.cb")))
extern osRtxThread_t uart_thread_tcb[1];

// Reserve two areas for the stacks of worker thread 1
// uint64_t makes sure the memory alignment is 8
extern uint64_t worker_thread_stk_2[128];

// Define the attributes which are used for thread creation
// Optional const saves RAM memory and includes the values in periodic ROM tests
extern const osThreadAttr_t worker_attr_2;

// Define ID object for thread 
extern osThreadId_t tid_uart;

extern uint16_t g_uart_data_length;

#define TH_FG_TX_DATA_READY    (0x0001)
#define TH_FG_RX_DATA_READY    (0x0002)

#define RX_BUFFER_SIZE 256
#define TX_BUFFER_SIZE 256
typedef struct
{
    volatile uint16_t dataLength;
    volatile uint16_t currCNDTR;
    volatile uint16_t prevCNDTR; /* Holds previous value of DMA_CNDTR */
} DMA_Event_t;

typedef struct{
    volatile uint16_t dataLength;
    uint8_t* pData;
} UART_Data_t;

#define APP_TX_DATA_SIZE 512
extern uint8_t UserTxBufferFS[APP_TX_DATA_SIZE];

extern volatile uint8_t mutex_uart_tx;
extern volatile uint8_t mutex_uart_rx;

extern DMA_Event_t dma_uart_rx;
extern UART_Data_t uart_data;
extern uint8_t rxBuffer[RX_BUFFER_SIZE];
extern uint8_t txBuffer[TX_BUFFER_SIZE];

extern volatile FIFO uart_rx_data_fifo;
extern volatile FIFO uart_tx_data_fifo;

void thread_uart(void *arg);

#endif // __THREAD_UART